const {v4: uuid} = require("uuid");

function createId(){
    const buf = Buffer.alloc(16);
    //create uuid in buffer
    uuid(null, buf, 0);
    return buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-').replace(/=/g,'');
}

module.exports = createId;