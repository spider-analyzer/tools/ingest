"use strict";

const bunyan = require('bunyan');

let log = null;

module.exports = (applicationName, level) => {
    if(!log && applicationName) {
        log = bunyan.createLogger({
            name: applicationName,
            streams: [
                {
                    stream: process.stdout,
                    level: process.env.LOG_LEVEL_STDOUT || level || bunyan.INFO
                }],
            serializers: bunyan.stdSerializers
        });
    }

    return log;
};