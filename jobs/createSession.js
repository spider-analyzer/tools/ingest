const Logger = require('../utils/logger');
const fetch = require('node-fetch');

async function createSession({spiderBaseUri, email, password, end}){

    const uri = `${spiderBaseUri}/customer/v1/sessions/`;
    const options = {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            email,
            password
        }),
        redirect: 'follow',
        timeout: 5000
    };
    const res = await fetch(uri, options);
    if(res.ok){
        Logger().info(`Successfully connected to Spider at ${spiderBaseUri} with ${email}`);
        const {token} = await res.json();
        return token;
    }
    else {
        Logger().error({status: res.status, body: await res.text()}, 'Wrong credentials or wrong spider Uri!')
        end();
    }
}

module.exports = {
    createSession
}