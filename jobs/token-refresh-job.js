"use strict";

const sleep = require('timers/promises').setTimeout;
const {DateTime, Duration} = require('luxon');
const jwtDecode = require('jwt-decode');

const Logger = require('../utils/logger');
const {createSession} = require("./createSession");

const percent = 0.9;
const delayForRecovery = 2*60*1000;

let job;

class TokenRefreshJob {

    constructor({email, password, end, token, spiderBaseUri}){
        this.isRunning = true;
        this.login = email;
        this.password = password;
        this.token = token;
        this.spiderBaseUri = spiderBaseUri;

        this.runner(end).catch(err => {
            Logger().error(err, `Token refresh job: Ended in error.`);
        });

        job = this;
    };

    computeDelay(){
        const log = Logger();
        const tokenDecoded = jwtDecode(this.token);
        const delayInS = (tokenDecoded.exp - DateTime.now().toSeconds()) * percent; //refresh at % of end of life
        this.delay = delayInS * 1000;
        log.debug(`Token refresh job: Should refresh token in ${Math.floor(delayInS/86400)}d ${Math.floor((delayInS%86400)/3600)}h ${Math.floor((delayInS%3600)/60)}min ${Math.round(delayInS%60)}s.`);
    }

    async runner(end){
        const log = Logger();
        while(this.isRunning){
            try{
                this.computeDelay();
                await sleep(this.delay);
                await this.getToken();
            }
            catch(err){
                log.error(`Token refresh job: Error while refreshing token.`);
                end({force: true});
            }
        }
    };

    async getToken(){
        const log = Logger();
        log.info(`Token refresh job: Calling spider to get token.`);
        this.token = await createSession({...this});
    }

    stop(){
        this.isRunning = false;
    }

    static getToken(){
        return job && job.token;
    }
}

module.exports = TokenRefreshJob;
