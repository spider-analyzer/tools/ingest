NAME         := ingest-tool
NPM_REGISTRY := $(shell npm config get registry)
DOCKER_REGISTRY ?= registry.gitlab.com/spider-analyzer/public-images/
NPM          := npm
DOCKER       := docker
GIT          := git
TAG          ?= latest
IMAGE        := $(DOCKER_REGISTRY)$(NAME):$(TAG)
NOCACHE      ?= 0
HELM_CHART_NAME := spider-ingest
HELM_REPO_NAME := floocus
KUBE_NAMESPACE := ingest

ifeq ($(NOCACHE), 1)
		DOCKER_BUILD_OPTS=--no-cache
endif

node_modules: package.json ## install node modules
	$(NPM) install --quiet --registry=$(NPM_REGISTRY)

image: ## build docker image
	$(DOCKER) build $(DOCKER_BUILD_OPTS)  --tag $(IMAGE) --build-arg registry=$(NPM_REGISTRY) --build-arg port=$(PORT) .

pushDocker: ## push docker image to repository
	$(DOCKER) push $(IMAGE)

package: ## pockage helmchart
	helm package ./helmchart

pushHelm: ## push Helmchart
	helm cm-push $(HELM_CHART_NAME)-*.tgz $(HELM_REPO_NAME)
	rm $(HELM_CHART_NAME)-*.tgz

deploy:
	helm upgrade $(HELM_CHART_NAME) -f ./values.yaml ./helmchart --namespace $(KUBE_NAMESPACE) --create-namespace --install

clean:
	rm -f $(HELM_CHART_NAME)-*.tgz
	rm -rf node_modules dist

delete:
	kubectl delete namespaces $(KUBE_NAMESPACE)


help: ## display help
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z._-]+:.*?## / {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help
