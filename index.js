#!/usr/bin/env node

const applicationName = 'ingest-Tool';

const _ = require('lodash');
const readFile = require('fs').promises.readFile;
const sleep = require('timers/promises').setTimeout;
const {DateTime} = require('luxon');

const Logger = require('./utils/logger');
const {getHttpComs} = require("./parts/getHttpComs");
const {processData} = require("./parts/processData");
const TokenRefreshJob = require("./jobs/token-refresh-job");
const {createSession} = require("./jobs/createSession");
const createId = require("./utils/createId");

const log = Logger(applicationName, 'INFO');
const progressInterval = 10;

const runToken = createId();
log.info(`You may find ingested data in Spider with this filter: "req.headers.x-spider-ingest-run:${runToken}".`);

const app = async () => {

  //load config from env (user/pass)
  const auth = {
    email: process.env.EMAIL,
    password: process.env.PASSWORD
  };

  //load config from file (starttime/stoptime?/query/whisperer/destination/replacement regexp)
  const config = JSON.parse(await readFile('./config.json'));
  //init regexps in config
  createRegexp(config);

  //init date range
  let stopDate = DateTime.fromISO(config.stopDate);
  const maxDate = stopDate;
  let startDate = DateTime.fromISO(config.startDate);

  log.info(`Will fetch data to inject from ${startDate} to ${stopDate}`);

  //init state for follow up
  let lastDate, deltaTimeProcessing, requestsInPipe = 0, requestsCount = 0,
      statusDiff = 0, initReportTime = DateTime.now(), lastReportTime= initReportTime, lastRequestsCount = 0,
      page=1, totalItems=0, next, lastDelay, similariesCount = 0, totalSimilarities = 0
  ;
  const addInPipe = () => requestsInPipe++;
  const removeFromPipe = () => requestsInPipe--;
  const addRequest = () => { return ++requestsCount };
  const addStatusDiff = () => statusDiff++;
  const addSimilarity = sim => {
    similariesCount++;
    totalSimilarities += sim;
  }
  const showProgress = ({fromStart, end, now} = {now: DateTime.now(), fromStart: false, end: false}) => {
    const deltaSec = ((now) - (fromStart ? initReportTime : lastReportTime))/1000;
    const deltaReq = fromStart ? requestsCount : requestsCount - lastRequestsCount;

    let msg = `${fromStart ? 'Total r' : 'R' }equests sent: ${requestsCount} at ${_.round(deltaReq/deltaSec, 1)}/sec`;
    msg += `\n  Matching statuses: ${requestsCount ? _.round((requestsCount-statusDiff)*100/requestsCount ,1) : 0}%`;
    msg += `\n  Similarity of response bodies: ${_.round(totalSimilarities*100/similariesCount,1)}%`;

    if(!end){
      msg += `\n  Requests in pipe: ${requestsInPipe}.`;
      msg += `\n  Last sent was at: ${lastDate?.toISO()}`;
    }

    log.info(msg);

    lastReportTime = DateTime.now();
    lastRequestsCount = requestsCount;
  }

  // Termination function
  const end = ({force} = {}) => {
    const now = DateTime.now();
    setTimeout(() => {
      log.info(`Injection ${force ? 'interrupted.' : 'finished.'}`);
      showProgress({fromStart: true, end: true, now});
      process.exit(0);
    }, 2000);
  }
  process.on('SIGTERM', end);

  //Spawn token refresh job to get token
  const token = await createSession({...config, ...auth});
  new TokenRefreshJob({...config, ...auth, token, end});

  log.info(`Showing status every ${progressInterval}s.`);
  const progressTimer = setInterval(showProgress, progressInterval*1000);

  do {
    //execute query to get data
    let items = [];
    try{
      const res = await getHttpComs({...config, startDate, stopDate, next});
      next = res.nextPage;
      items = res.items;
    }
    catch(e){
      log.info(`Failed getting items from Spider, will retry in 5s.`);
      await sleep(5 * 1000);
      continue;
    }

    if(!items.length){
      if(!deltaTimeProcessing && (startDate < DateTime.now().minus({'minutes': 1}))){
        log.info('No data to process, exiting.');
        process.exit(0);
      }
      else{
        //if pause in data, wait for 2s and loop
        log.info('Pause in data, waiting 2s.');

        //reset stopDate to maxDate to avoid blocking
        stopDate = maxDate;

        await sleep(2 * 1000);
        continue;
      }
    }

    totalItems+=items.length;
    log.debug(`Page ${page++}, items: ${items.length}, total items: ${items.length}`);

    //if first item, note initial date
    deltaTimeProcessing ??= DateTime.now() - DateTime.fromISO(items[0].req.startDate);

    //get last date to close loop if over limit
    lastDate = DateTime.fromISO(items[items.length-1].req.startDate);
    startDate = lastDate;
    stopDate = startDate.plus({minutes: 1}); // to fasten queries (limit sorting)

    //if data, spawn a promise to process delta
    processData({
      ...config,
      items: items.filter(i => DateTime.fromISO(i.req.startDate) < maxDate),
      deltaTimeProcessing,
      addInPipe,
      removeFromPipe,
      addRequest,
      addStatusDiff,
      addSimilarity,
      runToken
    })
        .then()
        .catch(e => log.error(e, 'Unexpected error!'));

    lastDelay = deltaTimeProcessing - (DateTime.now() - lastDate);

    //if last delay > 2s, it means we have too fast, wait delta - 1s before next page (auto regulation)
    if(lastDelay > 2000){
      log.debug(`Last com to send is in ${_.round(lastDelay/1000,1)}s. Pausing until then.`)
      await sleep(lastDelay - 1000);
    }
  }
  while(!lastDate || (lastDate < maxDate));

  clearInterval(progressTimer);

  while(requestsInPipe){
    log.info(`Still ${requestsInPipe} requests in progress.`)
    await sleep(5000);
  }

  end();
}

app().catch(err => {
  log.error(err, `Unexpected error!`);
  process.exit(1);
});

function createRegexp(config){
  [
    'pathsToIgnore',
    'headersToIgnore'
  ].forEach(
      key => config[key] = config[key].map(r => new RegExp(r))
  );

  [
      'urlReplacementsPatterns',
      'queryReplacementsPatterns',
      'headersReplacementsPatterns',
      'bodyReplacementsPatterns',
      'bodyValidationReplacementsPatterns'
  ].forEach(
      key => config[key] = config[key].map(r => ({match: new RegExp(r.match, 'g'), replace: r.replace}))
  );
}