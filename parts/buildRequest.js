const _ = require("lodash");
const {encode} = require("./encodeBody");

async function buildRequest({   item,
                          reqBody,
                          urlReplacementsPatterns,
                          queryReplacementsPatterns,
                          headersReplacementsPatterns,
                          bodyReplacementsPatterns,
                          headersToIgnore,
                          runToken
                      }){
    let {uri, query} = resolveLocation(item);
    uri = transform({content: uri, patterns: urlReplacementsPatterns});
    query = transform({content: query, patterns: queryReplacementsPatterns});
    const url = uri + query;

    const headersToKeep = _.pickBy(item.req.headers, headerValue => !shouldIgnore({content: headerValue, patterns: headersToIgnore}));
    const headers = _.mapValues(headersToKeep, headerValue => transform({content: headerValue, patterns: headersReplacementsPatterns }));
    const transformedBody = transform({content: reqBody, patterns: bodyReplacementsPatterns});
    const data = transformedBody ? await encode({content: transformedBody, encoding: headers['content-encoding']}) : undefined;

    delete headers['content-length'];
    delete headers['transfer-encoding'];
    headers['x-spider-ingest-run'] = runToken;

    return {
        method: item.req.method,
        url,
        headers,
        data,
        timeout: 10000,
        validateStatus: () => true
    }
}

function transform({content, patterns}){
    let res = content || "";
    for(let p of patterns){
        res = res.replaceAll(p.match, p.replace);
    }
    return res;
}

function resolveLocation(item){
    const uri = item.req.uri;
    const basePath = item.req.headers['x-forwarded-basepath'] || '';
    const query = item.req.query ? '?'+item.req.query : '';
    const hash = item.req.hash ? '#'+item.req.hash : '';
    const host = item.req.headers.host;
    const proto = item.req.headers['x-forwarded-proto'];

    return {
        uri: `${proto || 'https'}://${host}${basePath}${uri}`,
        query: `${query}${hash}`
    }
}

function shouldIgnore({content, patterns}){
    for(let r of patterns){
        if(r.test(content)){
            return true;
        }
    }
    return false;
}

module.exports = {
    buildRequest,
    shouldIgnore
}