const fetch = require('node-fetch');

const Logger = require('../utils/logger');
const TokenRefreshJob = require("../jobs/token-refresh-job");
const {DateTime} = require("luxon");

async function getHttpComs({spiderBaseUri, startDate, stopDate, whisperers, query, next }){

    const uri = `${spiderBaseUri}/web-read/v1/http-com/_search/`;
    const options = {
        method: 'POST',
        headers: {
            'authorization': `Bearer ${TokenRefreshJob.getToken()}`,
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            size: 100,
            whisperers,
            startDate: startDate.toISO(),
            stopDate: stopDate.toISO(),
            query: `(${query}) AND dateModified:<now-45s`,
            sort: [{
                key: "req.start",
                order: "asc"
            }],
            withContent: true,
            avoidTotalHits: true,
            next
        }),
        redirect: 'follow',
        timeout: 15000
    };
    const res = await fetch(uri, options);
    if(res.ok){
        const response = await res.json();
        return {
            items: response.items,
            nextPage: response.nextPage?.query.next
        };
    }
    else {
        Logger().error({status: res.status, body: await res.text()}, 'Could not get items!')
        throw new Error('Could not get items!')
    }
}

module.exports = {
    getHttpComs
}