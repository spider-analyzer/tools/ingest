const {promisify} = require('util');
const zlib = require('zlib');
const brotliCompress = promisify(zlib.brotliCompress);
const gzip = promisify(zlib.gzip);
const deflate = promisify(zlib.deflate);

const Logger = require("../utils/logger");

async function encode({content, encoding}){
    if(content?.length && encoding){
        Logger().trace(`Encoding back ${content.length} of data in ${encoding}`);
        switch(encoding){
            case 'br':
                return await brotliCompress(content);
            case 'gzip':
                return await gzip(content);
            case 'deflate':
                return await deflate(content);
            default:
                return content;
        }
    }
    else{
        return content;
    }
}

module.exports = {
    encode
}