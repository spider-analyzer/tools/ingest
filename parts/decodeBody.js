const {promisify} = require('util');
const zlib = require('zlib');
const brotliDecompress = promisify(zlib.brotliDecompress);
const gunzip = promisify(zlib.gunzip);
const inflate = promisify(zlib.inflate);

async function decode(part){
    if(!part?.body?.content){
        return null;
    }

    let body = Buffer.from(part.body.content, 'base64');

    if(part.headers['transfer-encoding'] === 'chunked'){
        body = parseChunkedEncoding(body);
    }

    if(body?.length){
        const contentEncoding = part.headers['content-encoding'];
        switch(contentEncoding){
            case 'br':
                return (await brotliDecompress(body)).toString();
            case 'gzip':
                return (await gunzip(body)).toString();
            case 'deflate':
                return (await inflate(body)).toString();
            default:
                return body.toString();
        }
    }
    else{
        return null;
    }
}

const ASCII = 'ascii';
function parseChunkedEncoding(body){
    //Get the length, cut the chunked part .. for each part | gather parts at the end
    let matches = body.slice(0,body.indexOf('\r\n')).toString(ASCII).match(/^([\dA-Fa-f]+)/);
    //We avoid processing the extensions, if you want them, get the raw body

    if(matches?.length){
        //Convert hexa to int
        let len = parseInt(matches[1],16);

        //Removes chunk length from body
        body = body.slice(matches[0].length + 2); //+2: \n\r

        //Prepare output for all chunks
        let slices = [];

        //Progress from chunk to chunk
        while(len > 0){
            //Add the chunk to output
            slices.push(body.slice(0,len));

            //Progress
            body = body.slice(len + 2); //+2: \n\r
            //If not enough bytes in body, we stop here
            if(body.length === 0){
                break;
            }

            //Get next chunk
            matches = body.slice(0,body.indexOf('\r\n')).toString(ASCII).match(/^([\dA-Fa-f]+)/);

            if(matches){
                len = parseInt(matches[1],16);
                body = body.slice(matches[0].length + 2); //+2: \r\n
            }
        }

        //Headers can follow
        let rest = body.slice(0).toString(ASCII).match(/^\s+(.+)$/);
        if(!rest){
            //Currently, we don't do anything with additional headers.
            //TODO: check what headers can be added
        }

        return Buffer.concat(slices)
    }
    else {
        //Here I don't really now what to do...
        return null;
    }
}

module.exports = {
    decode
}