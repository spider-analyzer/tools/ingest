function transformResponse({
                                resBody,
                                bodyValidationReplacementsPatterns
                            }){
    let res = resBody || "";
    for(let p of bodyValidationReplacementsPatterns){
        res = res.replaceAll(p.match, p.replace);
    }
    return res;
}

module.exports = {
    transformResponse,
}