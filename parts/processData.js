const {DateTime} = require('luxon');
const sleep = require('timers/promises').setTimeout;
const _ = require('lodash');
const {distance} = require('fastest-levenshtein');

const {decode} = require("./decodeBody");
const axios = require("axios");
const Logger = require("../utils/logger");
const {buildRequest, shouldIgnore} = require("./buildRequest");
const {transformResponse} = require("./transformResponse");

async function processData({   items,
                               deltaTimeProcessing,
                               addInPipe,
                               removeFromPipe,
                               addRequest,
                               addStatusDiff,
                               pathsToIgnore,
                               addSimilarity,
                               ...config
}){
    const log = Logger();

    //for each item
    await Promise.allSettled(items.map(async item => {
        let id;
        try{
            if(shouldIgnore({content: item.req.uri, patterns: pathsToIgnore})){
                return null;
            }

            //decompress / reassemble message
            let reqBody, originalResponse;
            reqBody = await decode(item.req);
            if(item.res?.body?.size){
                try{
                    originalResponse = await decode(item.res);
                }
                catch(e){
                    Logger().error(e, `Error when decompressing response body for request ${item.req.method} ${item.req.uri} at ${item.req.startDate}.`)
                    originalResponse = '';
                }
            }

            //if body is complete
            // perform replacement
            // construct message with same headers, replaced body
            const options = await buildRequest({...config, item, reqBody});

            // program message sending based on delta
            let delay = deltaTimeProcessing - (DateTime.now() - DateTime.fromISO(item.req.startDate)); //do your maths
            if(delay < 0) delay = 0;

            addInPipe();
            await sleep(delay);
            const t0 = DateTime.now();
            id = addRequest();
            log.trace(`#${id} - ${options.method} ${options.url} -> `);
            const res = await axios(options); //node-fetch does not support get with bodies
            const duration = DateTime.now() - t0;
            removeFromPipe();

            const originalStatusCode = item.stats.statusCode;
            const newStatusCode = res.status;
            const newResponse = res.data || '';

            log.trace(`#${id} - ${options.method} ${options.url}\n->  ${newStatusCode} - ${_.padStart(newResponse.length, 6)}B - ${_.padStart(_.round(duration), 4)}ms\nWas ${originalStatusCode} - ${_.padStart(originalResponse.length,6)}B - ${_.padStart(_.round(item.stats.duration*1000),4)}ms`);

            //compute diff score
            if(originalStatusCode !== newStatusCode) addStatusDiff();
            if(originalResponse !== newResponse){
                const sim = similarity(
                    transformResponse({
                        resBody: originalResponse,
                        ...config
                    }),
                    newResponse
                );
                addSimilarity(sim);
            }
            else if(originalResponse){
                addSimilarity(1);
            }

        }
        catch (e){
            Logger().error(e, `#${id}: Unexpected error for request ${item.req.method} ${item.req.uri} at ${item.req.startDate}.`)
            removeFromPipe();
        }
    }));
}

function similarity(before = '', after = ''){
    const len = _.max([before.length, after.length]);
    const dist = distance(before, after);
    return (len - dist) / len;
}


module.exports = {
    processData
}