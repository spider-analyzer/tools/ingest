# Build bundle

FROM node:18-alpine AS build_bundle

ARG registry
RUN apk --no-cache add python3 build-base
WORKDIR /app

COPY package*.json /app/
RUN npm install --quiet --no-progress --registry=${registry:-https://registry.npmjs.org}

COPY . /app
RUN npm run build \
    && npm cache clean --force \
    && npm prune --production

# Final

FROM alpine:3.17

RUN apk add nodejs &&\
    find /sbin /bin /usr/bin /usr/local/bin/ -type l  -exec busybox rm -rf {} \;; \
    busybox rm /sbin/apk /bin/busybox

WORKDIR /app
COPY --from=build_bundle /app/node_modules /app/node_modules
COPY --from=build_bundle /app/dist /app/

USER 1000:1000
ENTRYPOINT ["node"]
CMD ["index.js"]
